﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Stackholder
{
    public class ConnectionData
    {
        private string _DBconnString = WebConfigurationManager.ConnectionStrings["STAKEHOLDERSDB"].ToString();
        SqlConnection objConn = null;

        public  DataTable getStackholderBySector(String stDept  , String year)
        {
            SqlCommand sqlCmd = null;
            DataTable dt = null;
            
            try
            {


                sqlCmd = new SqlCommand();
                sqlCmd.CommandText = "sp_Ref_Business_Architecture";

                objConn = new SqlConnection(_DBconnString);
                objConn.Open();

                sqlCmd.Connection = objConn;
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.Add(new SqlParameter("@paraDesc2", SqlDbType.VarChar));
                sqlCmd.Parameters["@paraDesc2"].Value = stDept;

                sqlCmd.Parameters.Add(new SqlParameter("@paraYear", SqlDbType.VarChar));
                sqlCmd.Parameters["@paraYear"].Value = year;

           

                SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                dt = new DataTable("tbl");
                da.Fill(dt);

                sqlCmd.Dispose();
                objConn.Close();

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dt;
        }


        public DataTable getGroup(String strYear)
        {
            SqlCommand sqlCmd = null;
            DataTable dt = null;

            try
            {


                sqlCmd = new SqlCommand();
                sqlCmd.CommandText = "sp_SelectMainGroup";

                objConn = new SqlConnection(_DBconnString);
                objConn.Open();

                sqlCmd.Connection = objConn;
                sqlCmd.CommandType = CommandType.StoredProcedure;

              

                sqlCmd.Parameters.Add(new SqlParameter("@paraYear", SqlDbType.VarChar));
                sqlCmd.Parameters["@paraYear"].Value = strYear;



                SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                dt = new DataTable("tbl");
                da.Fill(dt);

                sqlCmd.Dispose();
                objConn.Close();

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dt;
        }

        public DataTable getSubGroup(String strYear, String mainID)
        {
            SqlCommand sqlCmd = null;
            DataTable dt = null;

            try
            {


                sqlCmd = new SqlCommand();
                sqlCmd.CommandText = "sp_SelectSubGroup";

                objConn = new SqlConnection(_DBconnString);
                objConn.Open();

                sqlCmd.Connection = objConn;
                sqlCmd.CommandType = CommandType.StoredProcedure;



                sqlCmd.Parameters.Add(new SqlParameter("@paraYear", SqlDbType.VarChar));
                sqlCmd.Parameters["@paraYear"].Value = strYear;

                sqlCmd.Parameters.Add(new SqlParameter("@MainGroupID", SqlDbType.VarChar));
                sqlCmd.Parameters["@MainGroupID"].Value = mainID;



                SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                dt = new DataTable("tbl");
                da.Fill(dt);

                sqlCmd.Dispose();
                objConn.Close();

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dt;
        }
    }
}