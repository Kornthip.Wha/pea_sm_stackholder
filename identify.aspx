﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="identify.aspx.cs" Inherits="Stackholder.identify" %>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <title>PEA-SM:</title>

        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="asset/css/custom.css" rel="stylesheet">
        <link rel="stylesheet" href="asset/icon/css/font-awesome.min.css">

         
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.13.5/css/dataTables.bootstrap5.min.css" rel="stylesheet">




        <script src="asset/js/bootstrap.bundle.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="js/app.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.js" ></script>
        <script src="https://cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js" ></script>
         <script src="https://cdn.datatables.net/1.13.5/js/dataTables.bootstrap5.min.js" ></script>

        <script src="https://cdn.datatables.net/fixedheader/3.2.3/js/dataTables.fixedHeader.min.js" ></script>
        <script src="https://cdn.datatables.net/fixedcolumns/4.3.0/js/dataTables.fixedColumns.min.js" ></script>       
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>




   <style>


   </style>

    </head>
    <body   onload="loadingData()">
        <div class="container-fluid d-flex min-vh-100 px-0 flex-no-warp">
            <div class="d-lg-flex d-none">
                <div class="d-flex flex-column flex-shrink-0 p-3 text-bg-dark" style="width: 280px;">
                    <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none">
                        <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"/></svg>
                        <span class="fs-4">PEA-SM จ้า</span>
                    </a>
                    <hr>
                    <ul class="nav nav-pills flex-column mb-auto">
                        <li class="nav-item">
                            <a href="#" class="nav-link active" aria-current="page">
                                <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#home"/></svg>
                                Home 
                            </a>
                        </li>
                        <li>
                            <a href="#" class="nav-link text-white">
                                <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#speedometer2"/></svg>
                                Dashboard
                            </a>
                        </li>
                        <li>
                            <a href="#" class="nav-link text-white">
                                <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#table"/></svg>
                                Orders
                            </a>
                        </li>
                        <li>
                            <a href="#" class="nav-link text-white">
                                <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#grid"/></svg>
                                Products
                            </a>
                        </li>
                        <li>
                            <a href="#" class="nav-link text-white">
                                <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"/></svg>
                                Customers
                            </a>
                        </li>
                    </ul>
                    <hr>
                    <div class="dropdown">
                        <a href="#" class="d-flex align-items-center text-white text-decoration-none dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="https://github.com/mdo.png" alt="" width="32" height="32" class="rounded-circle me-2">
                            <strong>mdo</strong>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-dark text-small shadow">
                            <li><a class="dropdown-item" href="#">New project...</a></li>
                            <li><a class="dropdown-item" href="#">Settings</a></li>
                            <li><a class="dropdown-item" href="#">Profile</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="#">Sign out</a></li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="flex-fill w-100">
                <!-- Navbar Moobile-->
                <nav class="navbar navbar-dark d-lg-none" id="navbarTopMobile">
                    <div class="navbar-brand">
                        PEA SM
                    </div>
                     <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mobileMenuContent" aria-controls="mobileMenuContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                     <div class="collapse navbar-collapse" id="mobileMenuContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="#">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Features</a>
                            </li>
                        </ul>
                    </div>
                </nav>
               <!-- End Navbar Moobile-->
               
                <!-- Navbar Desktop--->
                <nav class="navbar d-lg-flex d-none align-middle " id="navbarTop">
                    <div>PEA SM</div>
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link pr-0" href="#" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="ออกจากระบบ">
                                ออกจากระบบ
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- End Navbar Desktop-->
                
                
                
               
               
               <!-- Content-->
               <div class="container-fluid">

                   <br />
                   <!---- Table----->
                   <div id="dataDiv"  class="table-responsive p-3 bg-light "  >
				
                            <table id="example" class="table table-striped" style="font-size:0.8vw;font-family:Kanit">
                            <thead class="table-primary">
                              <tr>
               
                                 
                                   <th style="width:5%"  ></th>
                                    <th  style="width:2%" >ลำดับ</th>
                                   <th style="width:20%">กระบวนการ (level 0)</th>
                                    <th style="width:20%">กระบวนการ (level 1)</th>
                                    <th style="width:20%" >กระบวนการ (level 2)</th>
                                    <th style="width:15%" >ผู้มีส่วนได้ส่วนเสีย</th>
                                    <th  >กลุ่มหลัก </th>
                                    <th  >หลุ่มย่อย</th>
                                    <th  >I1 </th>
                                    <th  >I2 </th>
                                    <th  >I3 </th>
                                    <th  >I4 </th>
                                    <th  >D1 </th>
                                    <th  >D2 </th>
                                      <th  >D3 </th>
                            
                                    <th   >นัยสำคัญต่อ กฟภ.</th>
                          
                                  <th  >ความต้องการ/ความคาดหวัง</th>
                                  <th style="width:1%" >level and method</th>
                                  <th   ></th>
                                  
                          
                          

                                </tr>
                            </thead>
                        </table>
				       
                 </div>
                   
               </div>
               
               <!-- End  Content-->
            </div>



             <!------------ Modal Insert / Edit----------------->
		
            <button   id="btnModalTimeline"   type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter" style="display:none">
              
            </button>

            <!-- Modal -->
            <div class="modal fade bd-example-modal-xl" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true"   data-backdrop="static" data-keyboard="false" >
              <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">
                        <label id="lblProcess0"></label>
                    </h5>
                    
                  </div>
                  <div class="modal-body">
                     
                      <form>
                          <%--1--%>
                           <div class="row">
                                <div class="col">
                                  <label>กระบวนการ (level 1)</label>
                                </div>
                                <div class="col">
                                   <label>กระบวนการ (level 2)</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                   
                                  <textarea class="form-control" id="txtProcess1" rows="3" aria-label="Disabled input example" disabled></textarea>
                                </div>
                                <div class="col">
                                    <textarea class="form-control" id="txtProcess2" rows="3" aria-label="Disabled input example" disabled></textarea>
                                  <%--<input type="text" id="txtprocess21" class="form-control" placeholder="Last name" >--%>
                                </div>
                            </div>
                          <br />
                          <%--2--%>
                           <div class="row">
                               <div class="col">
                                <table>
                                    <tr>
                                        <td style="width:28%"><label>ประเภทผู้มีส่วนได้ส่วนเสีย : </label></td>
                                        <td style="width:20%;margin-right:2%">
                                             <select class="custom-select form-control" id="inputGroupSelect01" >
                                                <option selected>กรุณาเลือก...</option>
                                                <option value="1">ภายใน</option>
                                                <option value="2">ภายนอก</option> 
                                              </select>
                                        </td>
                                        <td style="width:35%"><input type="text" id="txtStackholderType" class="form-control ml-3" style="width:100%;" placeholder="ผู้มีส่วนได้ส่วนเสีย"></td>
                                    </tr>
                                </table>
                                </div>
                               <div class="col">
                                   <table>
                                       <tr>
                                           <td style="width:15%"><label>กลุ่มหลัก : </label></td>
                                           <td style="width:35%">
                                              <div id="divddlMainGroup"></div>
                                           </td>
                                           <td style="width:15%"><label>  กลุ่มย่อย : </label></td>
                                           <td style="width:35%">

                                               <div id="divddlSubGroup"></div>
                                           </td>
                                       </tr>
                                   </table>

                                </div>
                            </div>
                          <br />
                          <%--3--%>
                           <div class="row">
                                <div class="col">
                                    <table>
                                        <tr>
                                            <td colspan="6">คะแนน Impact  <i class="fa fa-info-circle" aria-hidden="true"></i> </td>
                                            <td colspan="4">คะแนน Dependency  <i class="fa fa-info-circle" aria-hidden="true"></i> </td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%">I1</td>
                                            <td style="width:10%">I2</td>
                                            <td style="width:10%">I3</td>
                                            <td style="width:10%">I4</td>
                                            <td style="width:10%">ค่าเฉลี่ย</td>
                                            <td style="width:10%"></td>
                                            <td style="width:10%">D1</td>
                                            <td style="width:10%">D2</td>
                                            <td style="width:10%">D3</td>
                                            <td style="width:10%">ค่าเฉลี่ย</td>
                                            <td style="width:10%"></td>

                                        </tr>
                                        <tr>
                                            <td style="width:10%"><input id="txtI1" type="number" class="form-control"  min="1" max="5" onchange="calAverage()" /></td>
                                            <td style="width:10%"><input id="txtI2" type="number" class="form-control"   min="1" max="5" onchange="calAverage()"/></td>
                                            <td style="width:10%"><input id="txtI3" type="number" class="form-control"   min="1" max="5" onchange="calAverage()"/></td>
                                            <td style="width:10%"><input id="txtI4" type="number" class="form-control"   min="1" max="5" onchange="calAverage()" /></td>
                                            <td style="width:10%"><input id="txtIAvg" type="text" class="form-control"    disabled/></td>
                                            <td style="width:0%"></td>
                                            <td style="width:10%"><input id="txtD1" type="number" class="form-control"   min="1" max="5" onchange="calAverage()" /></td>
                                            <td style="width:10%"><input id="txtD2" type="number" class="form-control"   min="1" max="5" onchange="calAverage()" /></td>
                                            <td style="width:10%"><input id="txtD3" type="number" class="form-control"   min="1" max="5" onchange="calAverage()"/></td>
                                            <td style="width:10%"><input id="txtDAvg" type="text" class="form-control"  disabled/></td>
                                            <td style="width:10%"></td>

                                        </tr>
                                    </table>

                                </div>
                                
                               
                                <div class="col">
                                    
                                     <table>
                                          <tr>
                                              <td  style="width:35%" >ความต้องการ/ความคาดหวัง</td>
                                              <td  style="width:60%" >
                                                  
                                                  <textarea class="form-control" id="txtrequest" rows="3" aria-label="Disabled input example"  style="" ></textarea>
                                              </td>
                                          </tr>
                                      </table>
                                    
                                 
                                </div>
                            </div>
                          <br />
                          <%--4--%>
                           <div class="row">
                                <div class="col">
                                    <div  class="row">
                                        <table style="margin-left:2%">
                                            <tr>
                                                <td ></td>
                                            </tr>
                                            <tr>
                                                <td style="width:25%">นัยสำคัญต่อ กฟภ.  : </td>
                                                <td  style="width:70%">
                                                    <input   id="txtSign"  type ="text" class="form-control"  style="width:28%" disabled>

                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                      
                                       
                                    
                                  
                                </div>
                                <div class="col">

                                  <div class="row">
                                         <table>
                                          <tr>
                                              <td  style="width:25%">level and method</td>
                                              <td  style="width:45%">
                                                 <%-- <input type="text" id="txtLvMethod" class="form-control" placeholder="level and method"  style="width:95%">--%>
                                                  <select class="custom-select form-control" id="ddlLvMethod" style="width:15%;margin-left:2%">
                                                    <option selected> - </option>
                                                    <option value="1">1</option>
                                                    <option value="3">3</option> 
                                                     <option value="5">5</option> 
                                                  </select>


                                              </td>
                                          </tr>
                                      </table>
                                     
                                       
                                     </div>
                                </div>
                            </div>
                          <br />
                          <%--5--%>
                           <div class="row">
                                <div class="col">
                                     <div class="row">

                                       

                                        
                                       
                                     </div>
                                   
                                </div>

                                <div class="col">
                                 
                                </div>
                            </div>
                          <br />
                         

                         

                        </form>




                  </div>
                  <div class="modal-footer">
                   
                     <div class="row">
                                <div class="col">
                                    <button type="button" class="btn btn-primary" onclick="setSignificant();">บันทึก</button>
                                    <button type="button" class="btn btn-secondary " data-dismiss="modal">ยกเลิก</button>
                                 </div>
                            </div>
                  </div>
                </div>
              </div>
            </div>

            
            
            
            
        </div>


<script type="text/javascript"> 

    function loadingData() {
       
        $(function () {
            $.ajax({
                type: "POST",
                url: "identify.aspx/getdata",
                data: '{"id": "1"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, status, xhr) {
                   // initMainGroup();

                    
                    table = $('#example').DataTable({
                        fixedColumns: {
                            left: 4
                        },
                        //paging: false,
                        bLengthChange: true,
                        scrollCollapse: true,
                        scrollY: "700px",
                        scrollX: true,
                        autoWidth: true,
                        lengthMenu: [[10, 20, -1], [10, 20, "All"]],
                        bFilter: true,
                        bSort: true,
                        bPaginate: true,
                        data: data.d,
                        columns: [

                            { "defaultContent": "<button type='button' class='btn btn-outline-secondary' id='btnAddNew' >+</button>", "orderable": false },
                            { "data": null, "orderable": false },
                            { "data": 'ProcessL0', "orderable": false },
                            { "data": 'ProcessL1', "orderable": false },
                            { "data": 'ProcessL2', "orderable": false },
                            { "data": 'StackHolderName', "orderable": false },
                            { "data": 'MainGroup', "orderable": false},
                            { "data": 'SubGroup', "orderable": false },
                            { "data": 'Impact1', "orderable": false},
                            { "data": 'Impact2', "orderable": false },
                            { "data": 'Impact3', "orderable": false },
                            { "data": 'Impact4', "orderable": false },
                            { "data": 'Dependency1', "orderable": false},
                            { "data": 'Dependency2', "orderable": false},
                            { "data": 'Dependency3', "orderable": false },

                            { "data": 'Significance', "orderable": false },
                            { "data": 'Request', "orderable": false},
                            { "data": 'LevelAndeMethod', "orderable": false},
                           
                            { "defaultContent": "<table> <tr> <td><button type='button' class='btn btn-outline-primary' id='btnEdit' ><i class='fa fa-pencil' aria-hidden='true'></i></button></td><td><button type='button' class='btn btn-outline-danger' id='btnRemove' ><i class='fa fa-trash-o' aria-hidden='true'></i></button></td> </tr></table>", "orderable": false },
                           
                           
                        ],

                        language: {
                            "lengthMenu": "แสดงข้อมูลหน้าละ  _MENU_ รายการ",
                            "zeroRecords": "ไม่พบข้อมูล",
                            "info": "หน้าที่  _PAGE_  จาก  _PAGES_  หน้า",
                            "infoEmpty": "ไม่พบข้อมูล",
                            "infoFiltered": "(ค้นหาจากทั้งหมด _MAX_ รายการ)",
                            "loadingRecords": "กำลังโหลดข้อมูล...",
                            "processing": "กำลังประมวลผล...",
                            "search": "ค้นหา :",
                            "paginate": {
                                "first": "แรก",
                                "last": "สุดท้าย",
                                "next": "ถัดไป",
                                "previous": "กลับ"
                            }
                        },
                        fnRowCallback: function (nRow, aData, iDisplayIndex) {

                            $('#example tbody').on('click', 'button', function () {
                                var data = table.row($(this).parents('tr')).data();

                               
                               
                                if (this.id != "btnRemove") {

                                    if (this.id == "btnAddNew") {
                                        clearInput();

                                        console.log(data.ProcessL1);


                                        document.getElementById("lblProcess0").innerText = data.ProcessL0;
                                        document.getElementById("txtProcess1").value = data.ProcessL1;
                                        document.getElementById("txtProcess2").value = data.ProcessL2;

                                        initMainGroup();
                                    }
                                    document.getElementById("btnModalTimeline").click();
                                }
                            });
                        }

                    });

                    table.on('search.dt', function () {
                        table.column(1, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                            cell.innerHTML = i + 1;
                        });

                    }).draw();
                

                },
                failure: function (response) {
                    $('#example').dataTable();
                    alert('failure  : ' + response.d);
                },
                error: function (response) {
                    $('#example').dataTable();
                    alert('error : ' + response.d);
                }
            });
        });

    }

    function initMainGroup() {

        var html = "";
        var subhtml = "";

        subhtml = "<select class='custom-select form-control' id='ddlMainGroup'   onchange='getSubGroup(this.value)'>";
        subhtml += "<option selected>กรุณาเลือก...</option>";
        subhtml += "</select>";
        document.getElementById("divddlSubGroup").innerHTML = subhtml;

        $(function () {
            $.ajax({
                type: "POST",
                url: "identify.aspx/getMainGroup",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, status, xhr) {

                   // alert(data.d.length);
                    html = "<select class='custom-select form-control' id='ddlMainGroup'   onchange='getSubGroup(this.value)'>";
                    html += "<option selected>กรุณาเลือก...</option>";
                    for (var i = 0; i < data.d.length;i++) {
                        html += "<option value='" + data.d[i].MainGroupName + "'>" + data.d[i].MainGroupName+"</option>";
                    }
                    html += "</select>";

                    
                        document.getElementById("divddlMainGroup").innerHTML = html;
                    
                    
                },
                failure: function (response) {
                    $('#example').dataTable();
                    alert('failure  : ' + response.d);
                },
                error: function (response) {
                    $('#example').dataTable();
                    alert('error : ' + response.d);
                }
            });
        });

    }

    function getSubGroup(id) {

        //alert(id);
        var html = "";

        $(function () {
            $.ajax({
                type: "POST",
                url: "identify.aspx/getSubGroup",
                data: '{"id":"'+id+'"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, status, xhr) {

                    // alert(data.d.length);
                    html = "<select class='custom-select form-control' id='ddlSubGroup' >";
                    html += "<option selected>กรุณาเลือก...</option>";
                    for (var i = 0; i < data.d.length; i++) {
                        html += "<option value='" + data.d[i].SubGroupName + "'>" + data.d[i].SubGroupName + "</option>";
                    }
                    html += "</select>";

                    document.getElementById("divddlSubGroup").innerHTML = html;
                },
                failure: function (response) {
                    $('#example').dataTable();
                    alert('failure  : ' + response.d);
                },
                error: function (response) {
                    $('#example').dataTable();
                    alert('error : ' + response.d);
                }
            });
        });

    }

    function calAverage() {

        //txtI1
        var I1, I2, I3, I4, IAvg, D1, D2, D3, DAvg = "0";
        I1 = document.getElementById("txtI1").value;
        I2 = document.getElementById("txtI2").value;
        I3 = document.getElementById("txtI3").value;
        I4 = document.getElementById("txtI4").value;

        if (I1>0&&I2>0&&I3>0&&I4>0) {
            document.getElementById("txtIAvg").value = ((parseInt(I1) + parseInt(I2) + parseInt(I3) + parseInt(I4)) / 4).toFixed(1);
        }
       

        D1 = document.getElementById("txtD1").value;
        D2 = document.getElementById("txtD2").value;
        D3 = document.getElementById("txtD3").value;
        if (D1 > 0 && D2 > 0 && D3 > 0) {

            document.getElementById("txtDAvg").value = ((parseInt(D1) + parseInt(D2) + parseInt(D3)) / 3).toFixed(1);
        }

        if (document.getElementById("txtIAvg").value != "" && document.getElementById("txtDAvg").value != "") {
            setSignificant();
        }
        //setSignificant

    }

    function saveData() {


        var eMain = document.getElementById("ddlMainGroup");
        var mainGroupId = eMain.value;
       
        var eSub = document.getElementById("ddlSubGroup");
        var subGroupId = eSub.value;
       

        var eoutOrIn = document.getElementById("inputGroupSelect01");
        var outOrIn = eMain.value;

        var stackholderType = document.getElementById("txtStackholderType").value;

        var significant = document.getElementById("txtSign").value;
        var request = document.getElementById("txtrequest").value;

        var levelMethod = document.getElementById("txtLvMethod").value;

        var strI1 = document.getElementById("txtI1").value;
        var strI2 = document.getElementById("txtI2").value;
        var strI3 = document.getElementById("txtI3").value;
        var strI4 = document.getElementById("txtI4").value;
        var strAvgI = document.getElementById("txtIAvg").value;
        var strD1 = document.getElementById("txtD3").value;
        var strD2 = document.getElementById("txtD3").value;
        var strD3 = document.getElementById("txtD3").value;
        var strAvgD = document.getElementById("txtDAvg").value;


       


    }

    function setSignificant() {

       
        var avgI, avgD, signi = "";
        avgI = document.getElementById("txtIAvg").value;
        avgD = document.getElementById("txtDAvg").value;
        let I, D =  0.0;
        I = parseFloat(avgI);
        D = parseFloat(avgD);

      

        if ((I >= 1 && I < 3) && (D >= 1 && D < 3)) {
            signi = "D";
        } else if ((I >= 3 && I <= 5) && (D >= 3 && D <= 5)) {
            signi = "A";
        } else if ((I >= 3 && I <= 5) && (D >= 1 && D < 3)) {
            signi = "B";
        } else if ((I >= 1 && I < 3) && (D >= 3 && D <= 5)) {
            signi = "C";
        }

        
        //txtSign

        document.getElementById("txtSign").value = signi;

    }


    function clearInput() {
        

        var elements = document.getElementsByTagName("input");
        for (var ii = 0; ii < elements.length; ii++) {
            //number
            if (elements[ii].type == "text" || elements[ii].type == "number") {
                elements[ii].value = "";
            }
        }

        var elements1 = document.getElementsByTagName("textarea");
        for (var ii = 0; ii < elements1.length; ii++) {
            elements1[ii].value = "";           
        }
        //
    }

</script>


    </body>
</html>


