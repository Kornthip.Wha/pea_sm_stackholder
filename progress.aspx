﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="progress.aspx.cs" Inherits="Stackholder.progress" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <title>PEA-SM:</title>

        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="asset/css/custom.css" rel="stylesheet">
        <link rel="stylesheet" href="asset/icon/css/font-awesome.min.css">

         
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.13.5/css/dataTables.bootstrap5.min.css" rel="stylesheet">




        <script src="asset/js/bootstrap.bundle.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="js/app.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.js" ></script>
        <script src="https://cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js" ></script>
         <script src="https://cdn.datatables.net/1.13.5/js/dataTables.bootstrap5.min.js" ></script>

        <script src="https://cdn.datatables.net/fixedheader/3.2.3/js/dataTables.fixedHeader.min.js" ></script>
        <script src="https://cdn.datatables.net/fixedcolumns/4.3.0/js/dataTables.fixedColumns.min.js" ></script>       
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>




   <style>
       body{margin-top:20px;}
.timeline-steps {
    display: flex;
    justify-content: center;
    flex-wrap: wrap
}

.timeline-steps .timeline-step {
    align-items: center;
    display: flex;
    flex-direction: column;
    position: relative;
    margin: 1rem
}

@media (min-width:768px) {
        .timeline-steps .timeline-step:not(:last-child):after {
            content: "";
            display: block;
            border-top: .25rem dotted #3b82f6;
            width: 2.46rem;
            position: absolute;
            left: 7.5rem;
            top: .3125rem
        }
      
    }

    .timeline-steps .timeline-content {
        width: 8rem;
        text-align: center
    }

    .timeline-steps .timeline-content .inner-circle {
        border-radius: 1.5rem;
        height: 1rem;
        width: 1rem;
        display: inline-flex;
        align-items: center;
        justify-content: center;
        background-color: #3b82f6
    }

    .timeline-steps .timeline-content .inner-circle:before {
        content: "";
        background-color: #3b82f6;
        display: inline-block;
        height: 3rem;
        width: 3rem;
        min-width: 3rem;
        border-radius: 6.25rem;
        opacity: .5
    }

    /******  not current step*/
    .timeline-notCurrSteps {
    display: flex;
    justify-content: center;
    flex-wrap: wrap
}

.timeline-notCurrSteps .timeline-step {
    align-items: center;
    display: flex;
    flex-direction: column;
    position: relative;
    margin: 1rem
}

@media (min-width:768px) {
        .timeline-notCurrSteps .timeline-step:not(:last-child):after {
            content: "";
            display: block;
            border-top: .25rem dotted #f0f0f5;
            width: 3.46rem;
            position: absolute;
            left: 7.5rem;
            top: .3125rem
        }
       
    }

    .timeline-notCurrSteps .timeline-content {
        width: 8rem;
        text-align: center
    }

    .timeline-notCurrSteps .timeline-content .inner-circle {
        border-radius: 1.5rem;
        height: 1rem;
        width: 1rem;
        display: inline-flex;
        align-items: center;
        justify-content: center;
        background-color: #f0f0f5
    }

    .timeline-notCurrSteps .timeline-content .inner-circle:before {
        content: "";
        background-color: #f0f0f5;
        display: inline-block;
        height: 3rem;
        width: 3rem;
        min-width: 3rem;
        border-radius: 6.25rem;
        opacity: .5
    }

   </style>

    </head>
    <body   onload="loadingData()" style="font-family:Kanit">
        <div class="container-fluid d-flex min-vh-100 px-0 flex-no-warp">
            <div class="d-lg-flex d-none">
                <div class="d-flex flex-column flex-shrink-0 p-3 text-bg-dark" style="width: 280px;">
                    <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none">
                        <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"/></svg>
                        <span class="fs-4">PEA-SM จ้า</span>
                    </a>
                    <hr>
                    <ul class="nav nav-pills flex-column mb-auto">
                        <li class="nav-item">
                            <a href="#" class="nav-link active" aria-current="page">
                                <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#home"/></svg>
                                Home 
                            </a>
                        </li>
                        <li>
                            <a href="#" class="nav-link text-white">
                                <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#speedometer2"/></svg>
                                Dashboard
                            </a>
                        </li>
                        <li>
                            <a href="#" class="nav-link text-white">
                                <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#table"/></svg>
                                Orders
                            </a>
                        </li>
                        <li>
                            <a href="#" class="nav-link text-white">
                                <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#grid"/></svg>
                                Products
                            </a>
                        </li>
                        <li>
                            <a href="#" class="nav-link text-white">
                                <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"/></svg>
                                Customers
                            </a>
                        </li>
                    </ul>
                    <hr>
                    <div class="dropdown">
                        <a href="#" class="d-flex align-items-center text-white text-decoration-none dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="https://github.com/mdo.png" alt="" width="32" height="32" class="rounded-circle me-2">
                            <strong>mdo</strong>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-dark text-small shadow">
                            <li><a class="dropdown-item" href="#">New project...</a></li>
                            <li><a class="dropdown-item" href="#">Settings</a></li>
                            <li><a class="dropdown-item" href="#">Profile</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="#">Sign out</a></li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="flex-fill w-100">
                <!-- Navbar Moobile-->
                <nav class="navbar navbar-dark d-lg-none" id="navbarTopMobile">
                    <div class="navbar-brand">
                        PEA SM
                    </div>
                     <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mobileMenuContent" aria-controls="mobileMenuContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                     <div class="collapse navbar-collapse" id="mobileMenuContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="#">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Features</a>
                            </li>
                        </ul>
                    </div>
                </nav>
               <!-- End Navbar Moobile-->
               
                <!-- Navbar Desktop--->
                <nav class="navbar d-lg-flex d-none align-middle " id="navbarTop">
                    <div>PEA SM</div>
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link pr-0" href="#" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="ออกจากระบบ">
                                ออกจากระบบ
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- End Navbar Desktop-->
                
                
                
               
               <!----------------------- Content -------------------------------------------------------------------->
               <div class="container-fluid">

                   <br />
                   <!---- On Progress  ----->
                   <div >                      
                        <div class="row text-center justify-content-center mb-5">
                            <div class="col-xl-6 col-lg-8">
                                <h2 class="font-weight-bold">ON PROGRESS</h2>
                                <p class="text-muted"></p>
                            </div>
                        </div>

                        <div class="row" style="width:100%">
                            <div class="col" style="width:100%" >
                                <div class="timeline-steps aos-init aos-animate"  data-aos="fade-up">

                                    <div id="divStep1" class="timeline-step" >
                                        <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2003">
                                            <div class="inner-circle" style="background-color:lightgray"></div>
                                            <p class="h6 mt-3 mb-1"><label id="lblNameLv1"></label></p>
                                            <p class="h6 text-muted mb-0 mb-lg-0"><label id="lblPositionLv1"></label></p>
                                        </div>
                                    </div>
                                    <div id="divStep2" class="timeline-step">
                                        <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2004">
                                            <div class="inner-circle"></div>
                                            <p class="h6 mt-3 mb-1"><label id="lblNameLv2"></label></p>
                                            <p class="h6 text-muted mb-0 mb-lg-0"><label id="lblPositionLv2"></label></p>
                                        </div>
                                    </div>
                                    <div id="divStep3" class="timeline-step">
                                        <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2005">
                                            <div class="inner-circle"></div>
                                            <p class="h6 mt-3 mb-1"><label id="lblNameLv3"></label></p>
                                            <p class="h6 text-muted mb-0 mb-lg-0"><label id="lblPositionLv3"></label></p>
                                        </div>
                                    </div>
                                    <div id="divStep4" class="timeline-step">
                                        <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2010">
                                            <div class="inner-circle"></div>
                                            <p class="h6 mt-3 mb-1"><label id="lblNameLv4"></label></p>
                                            <p class="h6 text-muted mb-0 mb-lg-0"><label id="lblPositionLv4"></label></p>
                                        </div>
                                    </div>
                                    <div id="divStep5" class="timeline-step mb-0">
                                        <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2020">
                                            <div class="inner-circle"></div>
                                            <p class="h6 mt-3 mb-1"><label id="lblNameLv5"></label></p>
                                            <p class="h6 text-muted mb-0 mb-lg-0"><label id="lblPositionLv5"></label></p>
                                        </div>
                                    </div>
                                    <div id="divStep6" class="timeline-step">
                                        <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2004">
                                            <div class="inner-circle"></div>
                                            <p class="h6 mt-3 mb-1"><label id="lblNameLv6"></label></p>
                                            <p class="h6 text-muted mb-0 mb-lg-0"><label id="lblPositionLv6"></label></p>
                                        </div>
                                    </div>
                                     <div id="divStep7" class="timeline-step">
                                        <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2004">
                                            <div class="inner-circle"></div>
                                            <p class="h6 mt-3 mb-1"><label id="lblNameLv7"></label></p>
                                            <p class="h6 text-muted mb-0 mb-lg-0"><label id="lblPositionLv7"></label></p>
                                        </div>
                                    </div>
                                     <div id="divStep8" class="timeline-step">
                                        <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2004">
                                            <div class="inner-circle"></div>
                                            <p class="h6 mt-3 mb-1"><label id="lblNameLv8"></label></p>
                                            <p class="h6 text-muted mb-0 mb-lg-0"><label id="lblPositionLv8"></label></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   <!---- On Progress ----->

                   <br />
                   <!---- Table----->
                   <div id="dataDiv"  class="table-responsive p-3 bg-light "  >
				
                            <table id="example" class="table table-striped" style="font-size:0.9vw;font-family:Kanit">
                            <thead class="table-primary">
                              <tr>
                                  <%-- <th style="width:5%"  ></th>--%>
                                   <th  style="width:2%" >ลำดับ</th>
                                   <th style="width:20%">กระบวนการ (level 0)</th>
                                    <th style="width:20%">กระบวนการ (level 1)</th>
                                    <th style="width:20%" >กระบวนการ (level 2)</th>
                                    <th style="width:15%" >ผู้มีส่วนได้ส่วนเสีย</th>
                                    <th  >กลุ่มหลัก </th>
                                    <th  >หลุ่มย่อย</th>
                                    <th  >I1 </th>
                                    <th  >I2 </th>
                                    <th  >I3 </th>
                                    <th  >I4 </th>
                                    <th  >D1 </th>
                                    <th  >D2 </th>
                                      <th  >D3 </th>
                            
                                    <th   >นัยสำคัญต่อ กฟภ.</th>
                          
                                     <th  >ความต้องการ/ความคาดหวัง</th>
                                     <th style="width:1%" >level and method</th>
                                     <%--<th   ></th>--%>
                                </tr>
                            </thead>
                        </table>
				       
                 </div>
                  <!---- Table----->
                   

                   <div class="container mt-5 mb-5">
                      <div class="row justify-content-md-center"  >
                        <div class="col-md-auto">
                          <button id="btnFirm" type="button" class="btn btn-primary"  onclick="fnConfirm(this.id)"   >เห็นชอบ</button>
                        </div>
                        <div class="col-md-auto">
                         <button  id="btnReject" type ="button" class="btn btn-danger"  onclick="fnConfirm(this.id)"  >ตีกลับ</button>
                        </div>
                        
                      </div>
                    </div>

                  
               </div>
               
               <!-- End  Content-->
            </div>


            <!-- Button trigger modal -->
            <button id="btnModal" type="button" class="btn btn-primary" data-toggle="modal" style="display:none" data-target="#exampleModalCenter">
            </button>

            <!-- Modal -->
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" style="font-family:Kanit" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">ยืนยันการบันทึกข้อมูล ?</h5>
                    
                  </div>
                  <div class="modal-body  form-group">
                    
                      <textarea id="txtAreaReject" placeholder="ระบุเหตุผลการตีกลับ" class="form-control">

                      </textarea>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-primary">ตกลง</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
                   
                  </div>
                </div>
              </div>
            </div>

           
            
        </div>


<script type="text/javascript"> 
    //timeline - notCurrSteps timeline - step
    document.getElementById("divStep2").className = "timeline-notCurrSteps timeline-step";
    document.getElementById("divStep3").className = "timeline-notCurrSteps timeline-step";
    document.getElementById("divStep4").className = "timeline-notCurrSteps timeline-step";
    document.getElementById("divStep5").className = "timeline-notCurrSteps timeline-step";
    document.getElementById("divStep6").className = "timeline-notCurrSteps timeline-step";
    document.getElementById("divStep7").className = "timeline-notCurrSteps timeline-step";
    document.getElementById("divStep8").className = "timeline-notCurrSteps timeline-step";

    // lblNameLv1  lblPositionLv1  
    // test
    document.getElementById("lblNameLv1").innerText = "xxxx xxxxx";
    document.getElementById("lblNameLv2").innerText = "xxxx xxxxx";
    document.getElementById("lblNameLv3").innerText = "xxxx xxxxx";
    document.getElementById("lblNameLv4").innerText = "xxxx xxxxx";
    document.getElementById("lblNameLv5").innerText = "xxxx xxxxx";
    document.getElementById("lblNameLv6").innerText = "xxxx xxxxx";
    document.getElementById("lblNameLv7").innerText = "xxxx xxxxx";
    document.getElementById("lblNameLv8").innerText = "xxxx xxxxx";

    document.getElementById("lblPositionLv1").innerText = "ผู้กรอกข้อมูล";
    document.getElementById("lblPositionLv2").innerText = "อก.";
    document.getElementById("lblPositionLv3").innerText = "แอดมินฝ่าย";
    document.getElementById("lblPositionLv4").innerText = "อฝ.";
    document.getElementById("lblPositionLv5").innerText = "แอดมินเขต";
    document.getElementById("lblPositionLv6").innerText = "ผชก.";
    document.getElementById("lblPositionLv7").innerText = "แอดมินสายงาน";
    document.getElementById("lblPositionLv8").innerText = "รผก.";
    function loadingData() {
       
        $(function () {
            $.ajax({
                type: "POST",
                url: "identify.aspx/getdata",
                data: '{"id": "1"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, status, xhr) {
                   // initMainGroup();

                    
                    table = $('#example').DataTable({
                        fixedColumns: {
                            left: 4
                        },
                        //paging: false,
                        bLengthChange: true,
                        scrollCollapse: true,
                        scrollY: "500px",
                        scrollX: true,
                        autoWidth: true,
                        lengthMenu: [[10, 20, -1], [10, 20, "All"]],
                        bFilter: true,
                        bSort: false,
                        bPaginate: true,
                        data: data.d,
                        columns: [

                            //{ "defaultContent": "<button type='button' class='btn btn-outline-secondary' id='btnAddNew' >+</button>", "orderable": false },
                            { "data": null, "orderable": false },
                            { "data": 'ProcessL0', "orderable": false },
                            { "data": 'ProcessL1', "orderable": false },
                            { "data": 'ProcessL2', "orderable": false },
                            { "data": 'StackHolderName', "orderable": false },
                            { "data": 'MainGroup', "orderable": false},
                            { "data": 'SubGroup', "orderable": false },
                            { "data": 'Impact1', "orderable": false},
                            { "data": 'Impact2', "orderable": false },
                            { "data": 'Impact3', "orderable": false },
                            { "data": 'Impact4', "orderable": false },
                            { "data": 'Dependency1', "orderable": false},
                            { "data": 'Dependency2', "orderable": false},
                            { "data": 'Dependency3', "orderable": false },

                            { "data": 'Significance', "orderable": false },
                            { "data": 'Request', "orderable": false},
                            { "data": 'LevelAndeMethod', "orderable": false},
                           
                           // { "defaultContent": "<table> <tr> <td><button type='button' class='btn btn-outline-primary' id='btnEdit' ><i class='fa fa-pencil' aria-hidden='true'></i></button></td><td><button type='button' class='btn btn-outline-danger' id='btnRemove' ><i class='fa fa-trash-o' aria-hidden='true'></i></button></td> </tr></table>", "orderable": false },
                           
                           
                        ],

                        language: {
                            "lengthMenu": "แสดงข้อมูลหน้าละ  _MENU_ รายการ",
                            "zeroRecords": "ไม่พบข้อมูล",
                            "info": "หน้าที่  _PAGE_  จาก  _PAGES_  หน้า",
                            "infoEmpty": "ไม่พบข้อมูล",
                            "infoFiltered": "(ค้นหาจากทั้งหมด _MAX_ รายการ)",
                            "loadingRecords": "กำลังโหลดข้อมูล...",
                            "processing": "กำลังประมวลผล...",
                            "search": "ค้นหา :",
                            "paginate": {
                                "first": "แรก",
                                "last": "สุดท้าย",
                                "next": "ถัดไป",
                                "previous": "กลับ"
                            }
                        },
                        fnRowCallback: function (nRow, aData, iDisplayIndex) {

                            $('#example tbody').on('click', 'button', function () {
                                var data = table.row($(this).parents('tr')).data();

                               
                               
                                if (this.id != "btnRemove") {

                                    if (this.id == "btnAddNew") {
                                        clearInput();

                                        console.log(data.ProcessL1);


                                        document.getElementById("lblProcess0").innerText = data.ProcessL0;
                                        document.getElementById("txtProcess1").value = data.ProcessL1;
                                        document.getElementById("txtProcess2").value = data.ProcessL2;

                                        initMainGroup();
                                    }
                                    document.getElementById("btnModalTimeline").click();
                                }
                            });
                        }

                    });

                    table.on('search.dt', function () {
                        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                            cell.innerHTML = i + 1;
                        });

                    }).draw();
                

                },
                failure: function (response) {
                    $('#example').dataTable();
                    alert('failure  : ' + response.d);
                },
                error: function (response) {
                    $('#example').dataTable();
                    alert('error : ' + response.d);
                }
            });
        });

    }

    function fnConfirm(id) {

        //txtAreaReject
        
        if (id == "btnReject") {

            document.getElementById("txtAreaReject").style.display = "block";
        } else {
            document.getElementById("txtAreaReject").style.display = "none";
        }
        document.getElementById("btnModal").click();
    }

    
</script>


    </body>
</html>
