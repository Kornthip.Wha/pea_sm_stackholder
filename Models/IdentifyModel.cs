﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stackholder.Models
{
    public class IdentifyModel
    {
        public string CaseID { get; set; }
        public string ProcessL0 { get; set; }
        public string ProcessL1 { get; set; }
        public string ProcessL2 { get; set; }
        public string StackHolderName { get; set; }
        public string MainGroup { get; set; }
        public string SubGroup { get; set; }
        public string Impact1 { get; set; }
        public string Impact2 { get; set; }
        public string Impact3 { get; set; }
        public string Impact4 { get; set; }
        public string Dependency1 { get; set; }
        public string Dependency2 { get; set; }
        public string Dependency3 { get; set; }
        public string Significance { get; set; }
        public string Request { get; set; }

        public string LevelAndeMethod { get; set; }

    }
}