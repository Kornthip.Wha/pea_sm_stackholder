﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stackholder.Models
{
    public class GroupModels
    {
        public string MainGroupID { get; set; }
        public string MainGroupName { get; set; }
        public string SubGroupID { get; set; }
        public string SubGroupName { get; set; }
    }
}