﻿using Stackholder.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Stackholder
{
    public partial class identify : System.Web.UI.Page
    {
        private static ConnectionData data = new ConnectionData();
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        public static List<IdentifyModel> getdata(String id)

        {
            List<IdentifyModel> listData = new List<IdentifyModel>();
            try
            {
                String strDept = "ฝบพ";
                String strYear = "2023";
                DataTable dt = data.getStackholderBySector(strDept,strYear);

                foreach (DataRow row in dt.Rows)
                {
                    listData.Add(new IdentifyModel
                    {
                        CaseID = "",
                        Dependency1 = "",
                        Dependency2 = "",
                        Dependency3 = "",
                        Impact1 = "",
                        Impact2 = "",
                        Impact3 = "",
                        Impact4 = "",
                        LevelAndeMethod = "",
                        MainGroup = "",
                        SubGroup = "",
                         ProcessL0 = row["Level0"].ToString(),
                        ProcessL1 = row["Level1"].ToString(),
                        ProcessL2 = row["Level2"].ToString(),
                        Request = "",
                        Significance = "",
                        StackHolderName = ""

                    });
                }
                

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return listData;

        }


        [WebMethod]
        public static List<GroupModels> getMainGroup()

        {
            List<GroupModels> listData = new List<GroupModels>();
            try
            {
                DataTable dt = data.getGroup("2023");
                if (null != dt) {
                    foreach (DataRow row in dt.Rows)
                    {
                        listData.Add(
                            new GroupModels
                            {
                                MainGroupID = row["MainGroup"].ToString(),
                                MainGroupName = row["MainGroup"].ToString(),

                            }
                            );
                    }
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return listData;

        }


        [WebMethod]
        public static List<GroupModels> getSubGroup( String id)

        {
            List<GroupModels> listData = new List<GroupModels>();
            try
            {
                DataTable dt = data.getSubGroup("2023",id);
                foreach (DataRow row in dt.Rows)
                {
                    listData.Add(
                        new GroupModels
                        {
                            SubGroupID = row["SubGroupID"].ToString(),
                            SubGroupName = row["SubGroup"].ToString(),

                        });
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return listData;

        }


        [WebMethod]
        public static List<IdentifyModel> saveData(
            String outOrIn,
            String stackholderType,
            String mainGroupId,
            String subGroupId,
            String significant,
            String request,
            String levelMethod,
            String strI1,
            String strI2,
            String strI3,
            String strI4,
            String strAvgI,
            String strD1,
            String strD2,
            String strD3,
            String strAvgD

            )

        {

            List<IdentifyModel> listData = new List<IdentifyModel>();
            try {


               
                try
                {
                    String strDept = "ฝบพ";
                    String strYear = "2023";
                    DataTable dt = data.getStackholderBySector(strDept, strYear);
                    listData.Add(new IdentifyModel
                    {
                        CaseID = "1",
                        Dependency1 = strD1,
                        Dependency2 = strD2,
                        Dependency3 = strD3,
                        Impact1 = strI1,
                        Impact2 = strI2,
                        Impact3 = strI3,
                        Impact4 = strI4,
                        LevelAndeMethod = levelMethod,
                        MainGroup = mainGroupId,
                        SubGroup = subGroupId,
                        ProcessL0 = "",
                        ProcessL1 = "",
                        ProcessL2 = "",
                        Request = request,
                        Significance = significant,
                        StackHolderName = stackholderType

                    });

                    foreach (DataRow row in dt.Rows)
                    {
                        listData.Add(new IdentifyModel
                        {
                            CaseID = "",
                            Dependency1 = "",
                            Dependency2 = "",
                            Dependency3 = "",
                            Impact1 = "",
                            Impact2 = "",
                            Impact3 = "",
                            Impact4 = "",
                            LevelAndeMethod = "",
                            MainGroup = "",
                            SubGroup = "",
                            ProcessL0 = row["Level0"].ToString(),
                            ProcessL1 = row["Level1"].ToString(),
                            ProcessL2 = row["Level2"].ToString(),
                            Request = "",
                            Significance = "",
                            StackHolderName = ""

                        });
                    }


                }
                catch (Exception ex)
                {
                    ex.ToString();
                }

               
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return listData;

        }


    }
}